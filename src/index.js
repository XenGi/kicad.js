module.exports = {
    render: render
};

function render(data, canvas, options) {
    return defaults;
}

// default colors taken form KiCAD
let defaults = {
    'Fg': {'r': 255, 'g': 255, 'b': 255},
    'Bg': {'r': 0, 'g': 0, 'b': 0},
    'F.Cu': {'r': 132, 'g': 0, 'b': 0},
    'B.Cu': {'r': 0, 'g': 132, 'b': 0},
    'F.Adhes': {'r': 132, 'g': 0, 'b': 132},
    'B.Adhes': {'r': 0, 'g': 0, 'b': 132},
    'F.Paste': {'r': 132, 'g': 0, 'b': 0},
    'B.Paste': {'r': 0, 'g': 194, 'b': 194},
    'F.SilkS': {'r': 0, 'g': 132, 'b': 132},
    'B.SilkS': {'r': 132, 'g': 0, 'b': 132},
    'F.Mask': {'r': 132, 'g': 0, 'b': 132},
    'B.Mask': {'r': 132, 'g': 132, 'b': 0},
    'Dwgs.User': {'r': 194, 'g': 194, 'b': 194},
    'Cmts.User': {'r': 0, 'g': 0, 'b': 132},
    'Eco1.User': {'r': 0, 'g': 132, 'b': 0},
    'Eco2.user': {'r': 194, 'g': 194, 'b': 0},
    'Egde.Cuts': {'r': 194, 'g': 194, 'b': 0},
    'Margin': {'r': 194, 'g': 0, 'b': 194},
    'F.Crtyd': {'r': 132, 'g': 132, 'b': 132},
    'B.CrtYd': {'r': 0, 'g': 0, 'b': 0},
    'F.Fab': {'r': 194, 'g': 194, 'b': 0},
    'B.Fab': {'r': 132, 'g': 0, 'b': 0},
    'grid': 1.27,
    'grid_color': {'r': 132, 'g': 132, 'b': 132}
};
